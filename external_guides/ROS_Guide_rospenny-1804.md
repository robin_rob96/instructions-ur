# ROS Guide rospenny-1804 (IPv4 `134.130.61.167` & `10.116.0.17`)

Rospenny-1804 runs Ubuntu 18.04 with the appropriate ROS version melodic.  
The catkin workspace is located under `~/catkin_ws` and needs to be sourced after opening a new terminal.  

    source ~/catkin_ws/devel/setup.bash

Installed packages:
 - ur_modern_driver https://git.rwth-aachen.de/fabian.wolff/ur_modern_driver (rospenny-1804 branch)
 - universal_robot https://git.rwth-aachen.de/fabian.wolff/universal_robot (rospenny-1804 branch)

Installed additional tools:
 - catkin_tools https://catkin-tools.readthedocs.io/en/latest/installing.html
 - rosdep https://docs.ros.org/en/independent/api/rosdep/html/overview.html

# UR5 (IPv4 `134.130.61.197`)

Make sure rospenny-1804 is set as ROS-Master in the `~/.bashrc` file when working exclusively with the UR5. Currently the connection via ROS only works if the IP on the UR5 is set to DHCP instead of chosing a static IP.

    export ROS_MASTER_URI=http://rospenny-1804:11311

## Moving the UR5 via MoveIt & RVIZ
Make sure that your workspace is sourced each terminal you start a launch file in. Otherwise ROS will use the files located under `/opt/ros/melodic/share/ur5_moveit_config` instead of the modified ones. Either source directly in the terminal or in the `~/.bashrc` file.

    source ~/catkin_ws/devel/setup.bash

### 1. ROS connection
Using the default launch filre from ur_modern_driver.

    roslaunch ur_modern_driver ur5_bringup.launch robot_ip:=134.130.61.197

### 2. MoveIt
Using the default launch files from universal_robot. 

    roslaunch ur5_moveit_config moveit_planning_execution.launch

### 3. RVIZ
Using the default launch files from universal_robot. 

    roslaunch ur5_moveit_config moveit_rviz.launch


# Summit XL (2/B) (IPv4 `10.116.0.18`)

Make sure the Summit is set as ROS-Master in the `~/.bashrc` file when working with the mobile platform.

    export ROS_MASTER_URI=http://10.116.0.18:11311

### SSH connection
    ssh summit@10.116.0.18
    R0b0tn1K

### Topics
- PS Controller: 
- Command Line: `/summit_xl/cmd_vel`
- twist_mux Out: `/summit_xl/summit_xl_control/cmd_vel`


## Turning on & moving the Summit
All buttons and switches are located at the back of the Summit. Go through the following individual steps in order.

- Plug the power cord into the power outlet or activate the power strip when already connected.
- Connect the battery to the charging cord (2 red, 2 black contacts)
- Reset the emergency stop (red knob)
- Turn on the battery (green ON/OFF selector switch)
- Turn on the CPU (square green button)

To control the robot using either method the safety light curtains need to be active (global red light on). A ROS multiplexer (twist_mux) is used to handle multiple control inputs from different control methods. The output of the multiplexer is published to the topic `/summit_xl/summit_xl_control/cmd_vel`.

### PS Controller
Select the PS controller with the label "Summit Steel Serial N°170421B" and turn it on. The connection should be made automatically. The controller commands are published to the topic `/summit_xl/pad_teleop/cmd_vel`.
- R1: safety button needs to be pressed during any movement
- left joystick: translational movement in the X-Y plane
- right joystick: rotation around the Z axis
- Triangle: increase movement speed by increments
- X: decrease movement speed by increments
- R2 + L2: activate/deactivate lateral movement

### Command line publishing
Velocity Commands can be directly published via command line to the topic `/summit_xl/cmd_vel`. Currently this option is only working when used in an SSH terminal on the Summit. After configurating the ROS network correctly this should be possible from the local machine as well.

    rostopic pub /summit_xl/cmd_vel

Values between `0.0` and `0.025` correspond to the slowest PS Controller movement speed setting.

    geometry_msgs/Twist "linear:
      x: 0.025
      y: 0.025
      z: 0.0
    angular:
      x: 0.0
      y: 0.0
      z: 0.025" -r 5
