# instructions-ur
## Basic nodes/ launch files

### launch usb_cam node with params
```
roslaunch camera_bringup camera_bringup.launch
```

Change usb cam: check available usb cam devices: ls /dev/video*

```
roslaunch camera_bringup camera_bringup.launch camera_device:=/dev/video4
```

### launch ar_track_alvar
```
roslaunch ar_track_alvar pr2_indiv_no_kinect.launch cam_image_topic:=/usb_cam/image_raw output_frame:=camera_frame cam_info_topic:=/usb_cam/camera_info
```

### run marker_id selector
```
rosrun ur_mover marker_id_selector.py
```

### run goal pub
```
rosrun  ur_mover ur_goal_pub.py
```

### echo published goal
```
rostopic echo -c /ur_goal_pose
```

### launch ur
```
roslaunch ur5_moveit_config demo.launch
```
---
## AR Tag chaser - let the ee move towards your ar tag position observed by the webcam
In three terminal windows run these commands. 
Select your desired marker id to observe in the second window.

```
roslaunch ur_mover ar_tag_chase.launch
rosrun ur_mover marker_id_selector.py
rosrun ur_mover ur_goal_pub.py
```

Hold the AR Tag towards the webcam and start the move_ur_cmd node. 
Remark: The position relative to camera is scaled down to make it easier to generate possible poses.

CAREFUL: Watch the planned trajectory first before pressing enter to avoid collision with the ground!

```
rosrun ur_mover move_ur_cmd.py
```

---
## Teaching a path for a planar glueing process
The camera watches from above towards your desk. You write a node that captures different poses of a specific ar tag.
At the end of capturing process, the robot moves that planar movement of your teached positions. 
Remark: You want to be able to set the height of movement plane (XY-plane) of the movement of your ee.


