# ROS Course Guide

This repo contains the markdown files for the ros course.

## Open Markdown Preview

In VS Code: CTRL + Shift + V

## Export Markdown as PDF

Install VSCode [extension](https://marketplace.visualstudio.com/items?itemName=yzane.markdown-pdf) Markdown PDF -> CTRL + SHIFT + P -> Markdown PDF: Export (pdf)
