![IGMR Banner](_doc/rwth_igmr_banner.png)

# ROS Noetic Project - Developing a welding application with ARTag-guided path planning

<img src="images/ur_welding.webp" width="300" height="300" title="UniversalRobot welding application">

[image source](https://www.migatronic.com/media/2995/optimise-welding-of-all-batch_600x800.jpg?center=0.89875,0.27166666666666667&mode=crop&width=600&height=600&rnd=132100828590000000&format=webp)

<font size="4">In this ROS project, you'll work in groups to perform a welding task with an Universal Robots 6-dof arm.
The objective is to plan and execute the 2D welding curve. The task for your team is to develop the complete application.

The desired path gets teached point-wise by ar tags that are filmed from a camera. In the best case, you will be able to generate a weld like the one shown below using the ar tag teach-stick. The tasks built on each other. It is recommended to first implement a linear movement between two points and expand from there.

Our goal is that you can work simultanously on the project and learn a lot by this realistic welding application. This guide always mentions the Universal Robots 5 (UR5), but it's applicable to other robots from UR, of course! Adjust the commands according to your robot!
</font>

<img src="images/exercise-welding-path.png" width="600" height="450" title="Example welding path">

Example of a welding path

## Prerequisites

- [x] ROS1 Noetic (latest ROS1 version) installed on Ubuntu 20.04
- [x] Necessary ROS packages are already installed. If not, they're available in the *opsi-client-kiosk*.
- [x] *Terminator* or the 'standard' ubuntu terminal + basic understanding
- [x] VSCode for programming
- [x] Some knowledge in programming (ideally using Python or C++)
- [x] **search** for information in the official documentation, forums, tutorials, github issues or simply review source code
- [x]  Beneficial: Have a high frustration tolerance when learning ROS, as the learning curve initially tends to be shallow. Additionally, it is important to engage in extensive experimentation and enjoy working as a team.
- [x] You can access to the additional files for the tasks in the share folder.

## Tasks summary

- [ ] Start movegroup for simple planning & execution of the ur (check drivers & visualisation in RViz)
- [ ] Use a camera driver & publish the video output in a topic
- [ ] Utilize ar_track_alvar for pose estimations of ARTags
- [ ] set up the test stand with mounted camera
- [ ] camera calibration
- [ ] write a node that uses the pose estimates for teaching a path using the ARTags (you might want to check if the pose is reachable)
- [ ] plan & execute the teached path (if possible)
- [ ] implement safety features (avoid collision with the ground e.g.)
- [ ] test & implement measures for a robust process
- [ ] implement additional features, e.g. set the end effector orientation by interpolating between 2 angles

## Learning outcomes

- [ ] **Collaboration and teamwork in a project-based environment**
- [ ] understand and increase understanding of how ROS works (topics, nodes, packages)
- [ ] controlling a robotic arm with ROS
- [ ] using external packages for hardware (camera, ARTags)
- [ ] learn the basics of path planning with MoveIt
- [ ] visualization with rviz

## Table of content

- [Task 1 - Test robot connection & first movement](#test-robot)
- [Task 2 - Embed a webcam in your ROS program](#webcam)
- [Task 3 - Tracking of AR Tags](#ar_track_alvar)
- [Task 4 - Implement safety features](#safety-features)
- [Task 5 - Teaching Node](#teaching-node)
- [Task 6 - Movement action server](#movement_action_server)
- [Task 7 - Merging all components](#merger)

---

<div style="page-break-after: always;"></div>

# Task 1 - Test robot connection & first movement <a name="test-robot"></a>

## 1. RViz + MoveIt demonstration

We first simulate a robot to plan and execute a trajectory with MoveIt.
Open your terminal, source ROS, build and source your workspace:

```
source /opt/ros/noetic/setup.bash

cd ~/ws_igmr/

catkin build
source devel/setup.bash
```
Afterwards, launch the ur5 demo-launchfile:
```
roslaunch ur5_moveit_config demo.launch use_rviz:=true
```
You should see an ur5 arm. In the left pannel, press *Add* and select *Trajectory* if it's not already added to your pannel. Drag the ball at the robot's end effector with your mouse to a desired position (See the image below).

<img src="images/rviz_movegroup_first_steps.png" width="740" height="422" title="UniversalRobot welding application">

In the pannel, press *Plan*. You should see the planned trajectory. (Select *Loop Animation* in the Trajectory menue inside the left pannel to see the planned trajectory again).

After a certain trajectory is planned, execute it with the button *Execute*.
That was your first movement of a (simulated) robot with rviz. We'll repeat it with the real one now.

## 2. Safety notice

Read the following instructions carefully!
<ul>
  <li><font size="4" style="color:#e03149">Emergency Stop: Ensure that the emergency stop button is easily accessible and can be pressed at any time to halt the robot's movement.</font></li>
  <li><font size="4" style="color:#e03149">Collision Avoidance: Clear the workspace of any obstacles to avoid collisions. Be aware: The arm itself cannot collide with itself, but with the ground plate or the end effector, if they are not part of the urdf or planning scene. Always check the planned path in rviz before executing any motion.</font></li>
  <li><font size="4" style="color:#e03149">Always read the entire paragraph first before starting to implement it.</font></li>
</ul>

## 3. Gather information of your robot

Press the emergency switch, we don't want to move anything so far.
Make sure the controller has power.Press the *ON* button on the robot's teach pendant. The teach pendant might show an initialization window, press *abort* at the bottom bottm (see image below).

<img src="images/ps-annotated-init.jpg" width="600" height="450" title="Robot initialization screen">

On the home screen, press *about*:

<img src="images/ps-annotated-home.jpg" width="600" height="450" title="Home screen">

You can now identify your PolyScope version and your robot's ip in the network. In this case, it's version is **1.8.x** and the ip address is **134.130.61.197**. Make a note with these information.

<img src="images/ps-annotated-about.jpg" width="600" height="450" title="About screen">

## 4. Network

Make sure your laptop and the robot are connected to the same network (via ethernet).
You can test the connection with a simple ping on the ip address (replace with the correct address):

    ping 134.130.61.197

## 5. PolyScope setup

PolyScope is the robot software which differs among the provided arms.

**Follow only PolyScope-steps according to your version.**

### PolyScope >= 3.7

In this newer version of PolyScope, it's neccessary to install & run a programm on the robot itself to let ROS control it.
Download externalcontrol-1.x.x.urcap from [here](https://github.com/UniversalRobots/Universal_Robots_ExternalControl_URCap/releases)

In the installation process you need your laptop's ip (that machine that is running the ros driver).
Enter the following command in a terminal and look out for the interface eth0 (if you are connected to the network via ethernet).

    ifconfig 

Follow the [instructions](https://github.com/UniversalRobots/Universal_Robots_ROS_Driver/blob/master/ur_robot_driver/doc/install_urcap_cb3.md) to install & start the urcap program.

### PolyScope == 1.8.x

There is nothing to do, just check the network connection.
The robot is already in the correct state for ROS-controlling.

## 6. Initialize robot

Navigate back to the home screen click on *Roboter EINSTELLEN* (*configure robot*) and then *Roboter INITIALISIEREN* (*initialize robot*). If not done already, unlock the emergency button. Your screen should look like the image below:

<img src="images/ps-init.jpg" width="600" height="450" title="Robot initialization">

Press *Ein* (*On*) in the first row.
You now need to enable every joint. You can do so by press (and hold) *Auto*, **but watch out for collisions**! The robot stops immediately when you release the button.

You can also move each joint separately until the green dot appears. If your screen looks like the following image, press *OK*. The initialization process is now finished.

<img src="images/ps-init-finished.jpg" width="600" height="450" title="Robot initialization">

## 7. Bringup roboter driver

You already have the necessary drivers/ packages installed in your workspace ~/ws_igmr.
Since the commands differ slightly, read the README instructions carefully to launch the necessary nodes for your robot. As in the simulation, you should be able to drag the interactive marker with your mouse to a new position.

**BEFORE YOU START PLANNING/ EXECUTING** change the velocity & acceleration scaling down to 0.2 or something similar. You don't want to plan/ execute with 100% velocity/ acceleration in the beginning. Now, as in the simulation, you can *Plan* a movement and *Execute* it within the MotionPlanning widget in rviz.

<div style="page-break-after: always;"></div>

# Task 2 - Embed a webcam in your ROS stack<a name="webcam"></a>

You'll use a webcam to estimate the pose of an ARTag. To do so, we need to embed the video stream of our webcam in ROS.
We'll benefit from the rich ecosystem and use the package **usb_cam**, it's already installed on your ubuntu laptop.

- [ ] Skim the linked documentation
- [ ] Create a new ros package with a directory *config* and copy the file *camera_calibration.yaml* inside.
- [ ] Write a ROS launch file, which starts the necessary nodes (with its parameters) and shows the video stream in rviz. Save the finished rviz configuration so that you can use it in your launch file.

## HINTS

- You can find the documentation of that package [here](http://wiki.ros.org/usb_cam)
- You might want to implement some arguments in the launch file, so that your work is reusable in later tasks. Reasonable arguments could be:
  - id of your camera device (string)
  - launch rviz node (bool)
- Find a *default* camera calibration file in the provided course material.

<div style="page-break-after: always;"></div>

# Task 3 - Tracking of AR Tags <a name="ar_track_alvar"></a>

We use our camera implementation to estimate poses of ar tags. For this task we use the package ar_track_alvar. Since its not released as a binary, we need to clone & build it ourselves. Within your workspace, execute the following command:

    git clone -b noetic-devel https://github.com/ros-perception/ar_track_alvar.git src
It clones the source code of that package from the branch *noetic-devel* for our distro noetic (hence, *-b noetic-devel*) inside the folder *src*.
Now, build (and source) your workspace, then you are ready to work with ar_track_alvar.

- [ ] Skim the linked documentation
- [ ] Write a launch file which uses ar_track_alvar and your previous work from task 2. Rviz should launch with the widget *Marker* to display the ar tags.
- [ ] Echo the alvar-specific topics in an additional terminal window.

## HINTS

- The documentation can be found [here](http://wiki.ros.org/ar_track_alvar)

<div style="page-break-after: always;"></div>

# Task 4 - Implement safety features <a name="safety-features"></a>

An important part of our application are safety features. For example, we need to make sure that the robot will not collide with it's environment. While MoveIt takes care of self-collisions of the arm, we need to complete the planning scene with our welding nozzle and probably the robot's mounting plane to avoid collision with the nozzle or it's environment.

- [ ] Integrate the nozzle in Rviz and MoveIt
- [ ] Come up with a solution to counteract collisions with the mounting plane
- [ ] Evaluate your camera setup. Are collisions possible? Take measures against it.

## HINTS

- There are several possible solutions.
- MoveIt's planning scene is responsible for collision objects.
- The nozzle (stl) is provided in the course materials folder.

<div style="page-break-after: always;"></div>

# Task 5 - Teaching Node <a name="teaching-node"></a>

We can read pose estimations through some topics by now. It's time now to develop our sophisticated teaching system. The input is the constant stream of pose estimations (or none if there is no ar tag in the field-of-view).

- [ ] Write a node which captures user inputs for saving the current pose as a goal pose. You might need to make transformations of the frames.
- [ ] Make sure that the nozzle moves along the teached path with a given z-offset to the XY-plane.

## ADDITIONAL TASKS

- implement your solution in terms of stability checking, e.g. are the poses reachable
- Try to implement the possibility for the user to use an arc shape between some points instead of a straight line. How could this be implemented?
- In the teaching process, check if the goal pose for the robot is reachable. If not, print a feedback to the user and ignore this point.
- (Difficult) Extend your implementation so that the user can enter the cartesian nozzle speed between 2 captured poses.

## HINTS

- Hardware-dimensions are provided in the additional material.
- for better results in the pose estimation, it might be good to [re-calibrate](http://wiki.ros.org/camera_calibration) your camera once you placed it.
- There are different possible approaches for planning and possibly different possible solutions.
- The teaching adapter for the nozzle offsets in z-direction by 8 mm.

<div style="page-break-after: always;"></div>

# Task 6 - Movement action server <a name="movement_action_server"></a>

The planning and execution is done in this node. It acts as an action server which gets the poses as inputs and plans/ executes the motion afterwards.

- [ ] create a new package for planning/ execution
- [ ] Implement an action server which plans & executes the movement given n valid poses.
- [ ] Write a little test node that can call this server for testing purpose.

## HINTS

- Planning & execution can be done with the [MoveGroupCommander Python Interface](http://docs.ros.org/en/jade/api/moveit_commander/html/classmoveit__commander_1_1move__group_1_1MoveGroupCommander.html)
- We have not much time to check the planning for collision, therefore: Make sure that no collision can happen (welding nozzle, ground plane, camera setup)
- A example welding path can be find in the provided course materials.

<div style="page-break-after: always;"></div>

# Task 7 - Merging all components <a name="merger"></a>

Congratulations, the hardest part is done! Now merge your work so that it can be launched with a single command.

- [ ] Merge your work into one launch file & test your application for edge cases.
