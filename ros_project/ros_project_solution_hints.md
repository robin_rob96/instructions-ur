# Solution hints for the ROS 'Developing a welding application with ARTag-guided path planning'

The full source code can be found here: XXX

## Task 2 - Embed a webcam in your ROS stack

- For testing purpose, use the default calibration settings *camera_calibration.yaml*
- Identify the video device id of your webcam. Use ls /dev/video* before and after connecting the webcam to see the difference.
- You need to define a frame (relative to world or map) which defines the position of the camera. For that, use
- the packag is called *usb_cam*, the executable *usb_cam_node*.
- In Rviz, make sure to select the *camera_frame* as fixed frame, if there is no transformation from world to your camera_frame.
- ```<node pkg="usb_cam" type="usb_cam_node" name="usb_cam" output="screen"> arguments... </node>```
- arguments are: video_device, camera_name, camera_frame_id, camera_frame_id
- it is not necessary to provide tf (static) transforms for the camera_frame, but you have to set the parameter *camera_frame_id* of the *usb_cam_node*

## Task 3 - Tracking of AR Tags

- launch camera_bringup.launch from task 2:

`roslaunch camera_bringup camera_bringup.launch`

```roslaunch ar_track_alvar pr2_indiv_no_kinect.launch cam_image_topic:=/usb_cam/image_raw output_frame:=camera_frame cam_info_topic:=/usb_cam/camera_info```

- In Rviz, add the widget *Marker*

`rostopic echo -c /ar_pose_marker`

## Task 4 - Implement safety features

- [PlanningSceneInterface Tutorial](https://ros-planning.github.io/moveit_tutorials/doc/move_group_python_interface/move_group_python_interface_tutorial.html)
- There was no good documentation, see implementation [here](http://docs.ros.org/en/jade/api/moveit_commander/html/planning__scene__interface_8py_source.html)
- Add a flat box to the planning scene to prevent collision with ground plane:

```
moveit_commander.roscpp_initialize(sys.argv)
scene = moveit_commander.PlanningSceneInterface()

box_pose = PoseStamped()
box_pose.header.frame_id = "base_link"
box_pose.pose.position = Point(0,0,0.01)
box_pose.pose.orientation.w = 1.0
scene.add_box("ground_plane", box_pose, size=(3,3, 0.01))
```

- The welding nozzle gets attached to the tcp of the robot:

```
rospack = rospkg.RosPack()
path = rospack.get_path("ur_mover") + "/meshes/nozzle_ur2.stl"

pose_stamped = PoseStamped()
pose_stamped.header.frame_id = "tool0"
pose_stamped.pose.position.z = 0.01


q_new = tf_conversions.transformations.quaternion_from_euler(0,math.pi,0)
q = Quaternion(*q_new)

# pose_stamped.pose.orientation.w = 1.0
pose_stamped.pose.orientation = q

scene.attach_mesh(
    name="welding_gun",
    pose=pose_stamped,
    filename=path,
    link="tool0",
    size=(0.001, 0.001, 0.001)

)
```
